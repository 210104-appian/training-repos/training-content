# SQL

RDBMS - DBeaver

### Relational Databases

- table based architecture, with columns of the tables relating to one another (FK relationships)
- primary key used to uniquely identify each row in a table - column must be unique and not null
  - candidate key is any candidate for a primary key
  - composite key is when a primary key is made up of two columns rather than just one
- when you define a table, you define the data type of the column, but you can also place additional restrictions on the data using constraints (fk, pk, unique, not null, check)
- data integrity - changes to db follow specifications of the db (types, constraints)
- referential integrity - changes to db maintain relationships specified in our db (can't delete records being referenced, cannot reference an entity or record that doesn't exist, etc)

## SQL

- Structured Query Language, means for interacting with our DB
- [Oracle 12c data types](https://docs.oracle.com/database/121/SQLQR/sqlqr06002.htm#SQLQR959)

### DDL - Data Definition Language

- defines the structure of our db, manages db entities
- CREATE, ALTER, DROP, TRUNCATE

### DML - Data Manipulation Language

- CRUD operations for data
- INSERT, SELECT, UPDATE, DELETE

#### DQL - Data Query Language

- used for querying data - SELECT keyword

### DCL - Data Control Language

- allows you to manage user permissions within your db
- GRANT, REVOKE

### TCL - Transaction Control Language

- allows you to explicitly create transactions
- BEGIN, COMMIT, ROLLBACK, SAVEPOINT

## Multiplicity

- One to One: profile to login credentials, passport and travelers \*passport references traveler
- One to Many(/Many to One): role to user \*role references user id
- Many to Many: sales to products, favorite an item \*both tables are referenced in a third table (join table/ junction table)

## Functions

**Aggregate functions** - performed on a data set and gives us one aggregated value

- sum, min, max, avg, count

**Scalar functions** - set number of arguments that return one value

- length(str), curdate(), concat(str1, str2), round, sqrt

## Query clauses

select [columns]

from [table]

where [condition] -- filters pre aggregation if aggregation is occurring

group by [column] -- defines how our data is going to be aggregated

having [condition] -- filters after aggregation

order by [column][asc/desc]

## Joins

- allows us to create a single query which spans across multiple tables in our db
- inner, left, right, full, natural

select [columns]

from [left table]

left/right/full join [right table]

on [join predicate];

## Transactions

- unit of work on a db, one or more related operations executed as a unit

#### Properties of a Transaction

**Atomicity**

- transactions executes in its entirety or not at all

**Consistency**

- db is in a consistent state before and after a transaction executes
- transaction maintains data and referential integrity

**Isolation**

- concurrent transactions should not affect the execution of one another

**Durable**

- committed data is permanent and cannot be rolled back

#### Transaction Isolation

- [transaction isolation](https://www.postgresql.org/docs/9.5/transaction-iso.html)

**Transaction phenomena**

1. Dirty Read

   a) begins a transaction

   a) inserts new user: id=5, name=Paul (uncommitted data)

   b) queries user table (reads uncommitted data)

   a) rolls back

- transaction b read uncommitted data

2. Non Repeatable Read

   a) begins a transaction

   a) queries user table

   b) begins a transaction

   b) updates user 7: changes email from joeshmo@gmail.com to josephshmo@gmail.com

   b) commits transaction

   a) (still in the same transaction) queries user table

- transaction a sees inconsistent result sets, records from the queries have fields which have changed

3. Phantom Read

   a) begins a transaction

   a) queries user table

   b) begins a transaction

   b) remove user 12

   b) commits transaction

   a) (still in the same transaction) queries user table, result set has one less record than first query

- transaction a sees an inconsistent result set, records have been added or removed in the time a has been executing

## Set Operations

- allows you to perform operations on two data sets, aggregating the records of the data set
- while joins work on combining the columns of two tables, [set operations](https://docs.oracle.com/cd/B19306_01/server.102/b14200/operators005.htm) combine the records of two data sets
- UNION and UNION ALL are used to combine two data sets, combining the records that result from the first with the records that result from the second - UNION will not show duplicate records that are present in result sets, while UNION ALL will show duplicates (UNION ALL is faster)
- INTERSECT show only the records that are common to both data sets
- MINUS shows the records from the first data sets, but excludes any records which are common to both data sets

```sql
[QUERY1]
UNION
[QUERY2]
```

```sql
[QUERY1]
UNION ALL
[QUERY2]
```

```sql
[QUERY1]
INTERSECT
[QUERY2]
```

```sql
[QUERY1]
MINUS
[QUERY2]
```

## Views

- logical, or virtual, table based on a data set
- contains no data itself, essentially represents a stored query

```sql
CREATE VIEW [NAME] AS
[QUERY]
```

## SubQueries

- a nested query within another expression
