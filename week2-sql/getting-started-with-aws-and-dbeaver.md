Connecting SQL Developer to an AWS Instance

0. Before you begin, you must have DBeaver Installed on your computer. If you do not, download the community edition [here](https://dbeaver.io/download/).

1. Create an account with [Amazon Web Services](https://portal.aws.amazon.com/billing/signup#/start). This will require you to enter your credit card information for billing. However, we will only be using functionality on the free tier of AWS. The account can sometimes take up to 24 hours to fully activate.

2. From the AWS Console, click on the Services tab on the nav bar and then on “Relational Database Service” under the “Database” header.

3. Click on the Databases tab on the left hand nav bar and then on the “Create Database” button.

4. When on the Create database page, choose “Standard create” as the database creation method. Select “Oracle SQL” for your engine, and “Oracle Standard Edition”. When given the option for a Template, select the “Free Tier”option -- this is important, you don’t want to get charged!

5. Name your database with the DB instance identifier, and create an admin user. Make sure to take note of these credentials. Under DB instance size, “db.t3.micro” should be selected - make sure not to change this.

6. Under Connectivity set “Public access” as “Yes”, and select “Create new” for the VPC security group. Give the security group a name (e.g. wfh-revature).

7. Leave the remaining advanced settings as their default, click launch database. You will see that your instance is creating. It will take some time for the status to become available.

   Open DBeaver. If it does not immediately prompt you to create a connection, click on the little plug at the top of the database navigator. Select Oracle for the type. The Host is the endpoint associated with your RDS (can be found under Databases tab when the database is selected and it’s become available). User and password are those associated with the credentials you set up when creating the RDS instance. Click Test Connection to make sure the connection can be established. When that is successful, click connect. (If you have a connection timeout issue, you likely have an issue with public accessibility or security groups)

On security groups:

The security group associated with your RDS instance control the network traffic allowed to it. When you create a new security group while creating your RDS, it should allow traffic from your IP address. If this is not the case, or your IP address changes, you'll want to update your security group. To do this follow these steps: - Click on the instance on the RDS dashboard to see its configuration. Under the “Connectivity & security” you should be able to click on the VPC security group you created. Once you select the security group, you can scroll down to the instance details and select the “Inbound rules” tab. Edit the inbound rule, setting the Type to RDS Oracle and the source to “My IP”. This will allow traffic from your ip address to access your DB. Keep in mind that if you intend to access your DB from another location, you will have to add your IP address to the security rule.

Another note on security: When we start working with JDBC later this week, we will be configuring our database credentials into our applications. We will be doing this configuration in a separate file and you will want to make sure that this file is never uploaded to GitHub (or if it is, the credentials have been removed)
