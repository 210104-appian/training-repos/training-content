# Some helpful git resources:

[Git cheat sheet](https://github.github.com/training-kit/downloads/github-git-cheat-sheet/)

[Tool which allows you to visualize git commands](https://learngitbranching.js.org/?locale=en_US)

- take a look at the "Remote" goals in the first modal

# Creating a Repository

Within your subgroup, you have owner permissions, and have the ability to create and manage your own repositories. There are two approaches to creating a local repository which is configured with a remote repository on GitLab.

1. Gitlab Project First:

   > git clone https://[repo link].git

   > cd [repo folder]

   > touch README.md

   > git add README.md

   > git commit -m "add README"

   > git push -u origin master

2. Local repository first

   > cd existing_folder

   > git init

   > git remote add origin https://[repo link].git

   > git add .

   > git commit -m "Initial commit"

   > git push -u origin master

# Pushing additional changes to your remote repository.

> git add [. -A or a particular file/directory]

> git commit -m "commit message"

> git push

# Pulling Notes/Code from the training content repo

Clone the remote repository to create a copy of it locally:

> git clone https://gitlab.com/210104-appian/training-repos/training-content.git

Navigating into the "training-content" folder, will give you the contents of the repository.

If you need to update the training-content repository, you can use `git pull` to pull any additional commits.

# Creating Your Own Branch on Demo Repositories

Clone the remote repository to create a copy of it locally:

> git clone https://gitlab.com/210104-appian/training-repos/java-bakery.git

Create a local branch to work on:

> git checkout -b [your branch]

# Want to Propose an Addition to the Master Branch? Open a pull request!

If you have changes you'd like to add to the master branch, start by creating a branch off of the master branch. Name this branch according to the feature or change you're making (e.g. bakery-item-refactor, adding-croissant-model, etc.)

Then, add the changes you would like to make to your new branch. From there, use GitLab to open a pull request between the master branch and your new branch. Request me as a reviewer and we'll try to get your code merged in :)
