What makes a good software developer?
- data structures algorithms (technical foundation)
- team work 
- practicality (thinking about your solution - is it maintainable? scalable? etc)
- goal oriented 
- adaptability, change with an adapting market 
- knowing where to look to solve problems
- how to ask questions effectively 
- communication
- hard work
- think outside the box, be ready to try multiple solutions
- debugging and problem solving
- documentation
- testing


Java
- object oriented programming
    - objects vs primitive
    - classes
    - wrapper classes
    - four pillars to describe oop: abstraction, encapsulation, inheritance, polymorphism
- create web applications
- OS independent - JVM
- garbage collection - automatic memory management
- compiled language
    - JDK (Java Development Kit) includes the compiler + JRE 
    - JRE (Java Runtime Environment) libraries + JVM
    - JVM (Java Virtual Machine) runs java bytecode (included in the JRE)
-> java compiler in the JDK converts java code to bytecode, which then is interpreted by the JVM 
- strongly typed 
- robust API 
- generics
- case sensitive, well established naming conventions
- mature, well documented, large community