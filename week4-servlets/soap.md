# SOAP - simple object access protocol

- client sends a SOAP message to a server
- client receives a SOAP message back

- service endpoint interface
  - an interface which outlines the methods which a client can interact with a SOAP service
- client is not the same as the browser; soap is often used for business to business communication
- all XML -> content type = text/xml
- in theory could use other transfer protocols (e.g. ftp, file transfer protocol)
- contract based, using the WSDL (Web Service Description Language)
- contract first = web service based on WSDL, contract last = web service created first

### SOAP Message

- message in XML
- envelope
  - header (opt)
  - body
    - fault (opt - in the case of a server error)

--> soap header/body not the same as http header/body

- entirety of soap message in http request/response body

### Fault

- an issue if something goes wrong (500 error)
- faultCode - text code indicating a class of errors
  - VersionMismatch (invalid SOAP envelope namespace)
  - Client (malformed information)
  - Server (problem with the server and message could not proceed, unavailable service)
- faultString - a text message explaining the error
- faultActor - specifies the URI associated with the error
- detail - application specific description of error

## WSDL (Web Service Description Language)

- xml representation of service endpoint interface
  - predefine all actions which can be taken over the service, as well as custom exceptions and complex types
- contract based
- contract first/ contrast last

### WSDL tags

- definitions
- types
- port
- message
- portType
- operation
- binding
- service

```xml
<definitions> <!-- root tag, includes our schema definition -->
    <types></types> <!-- defines complex types -->
    <message></message> <!-- describes data being communicated input/output types -->
    <message></message>
    <portType> <!-- describes the set of operations available at an endpoint  -->
        <operation></operation> <!-- what actions are available from our service endpoint interface, referencing input/output types -->
        <operation></operation>
    <portType>
    <binding></binding> <!-- describes binding style and use; how operations will be translated into xml -->
    <service> <!-- specify the collection of network ports -->
        <port></port> <!-- specifies the location of the service endpoint -->
    </service>
</definitions>
```

#### SOAP Binding

- describes how the service is bound to messaging protocol
- document or rpc (remote procedure call) style
  - rpc includes a tag with the method name with a child tag of each method argument
  - document has no assurance of xml structure, other than having one or more "parts" or child elements
- literal or encoded use
  - encoded contains data type information within the SOAP message
  - literal does not, relies on the schema
- for more on binding styles including examples see [this article](https://www.ibm.com/developerworks/library/ws-whichwsdl/index.html)

# Exposing/Consuming SOAP

- can do this using just java tools
- JAX-WS (Java API for XML Web Services/Jakarta XML Web Services) can be used to build soap clients and servers
- jaxws available with the jdk

### Consuming

#### Generating client code with WSDL:

- in your application (src/main/java)
  > wsimport [-Xnocompile] [location of wsdl]
