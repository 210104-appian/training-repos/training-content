# Servlets

- class that can handle HTTP requests

# Browser/Server communication HTTP

- hypertext transfer protocol
- defines how messages are formatted and transmitted across the network

## HTTP Request

- HTTP Method
- Version of HTTP protocol
- Request Headers (opt)
- URI
- Request Body (opt)

## HTTP Response

- Version of HTTP protocol
- Response Headers (opt)
- Response Body (opt)
- Response code
  - 100 informational
  - 200 success
  - 300 redirect
  - 400 client
  - 500 server

## HTTP METHODS

1. GET
   - used for retrieving data
   - append parameters to url
   - technically a body exists in the message, but any body would have no semantic meaning
   - limited in size of request
   - can be bookmarked
2. POST
   - used to create resources
   - cannot be bookmarked
   - information is sent in request body
3. PUT
   - used to update resources
4. DELETE
   - used to remove resources
5. HEAD
   - returns the headers that would be returned with a get request but without any of the actual body content
6. TRACE
   - used for diagnostics, echos back user input to user
7. OPTIONS
   - used to describe the communication options for the target resource

# Servlet Inheritance Tree

- Servlet (Interface)
  - init
  - service
  - destroy
- GenericServlet (Abstract Class)
  - service need be overriden, only abstract class
- HttpServlet (Abstract Class, no abstract methods)
  - implements service method
  - has handler methods for HTTP verbs, but implemented to return a 405

## Handler Methods

doXXX

```java
protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		// (typically) obtain request body, convert to a new record, and add to db
	}
```

# Deployment Descriptor

- WEB-INF > web.xml

```xml
<web-app>
    <context-param>
        <param-name>debug</param-name>
        <param-value>true</param-value>
    </context-param>
	<servlet>
        <init-param>
            <param-name>debug</param-name>
            <param-value>true</param-value>
        </init-param>
		<servlet-name>HelloWorldServlet</servlet-name>
		<servlet-class>com.revature.servlets.HelloWorldServlet</servlet-class>
        <load-on-startup>1</load-on-startup>
	</servlet>
	<servlet-mapping>
		<servlet-name>HelloWorldServlet</servlet-name>
		<url-pattern>/hello</url-pattern>
	</servlet-mapping>
</web-app>
```

### Request and Response Objects

Request

- Session
  - request.getSession() obtains the current session, if one does not exist it creates one
  - request.getSession(false) obtains the current session, if one does not exist it creates one
  - Session.setAttribute Session.getAttribute Session.getAttributeNames
  - Session.getId Session.getCreationTime Session.getLastAccessedTime
  - Session.invalidate()
- getMethod
- getHeader(name)
- getHeaders
- getRequestURI
- getRequestDispatcher
  - allows us to send our request and response objects to another servlets
  - forward
  - includes

Response

- getWriter (PrintWriter)
  - allows us to write into the response body
  - print, append
- sendRedirect
- addHeader
- getHeader
- getHeaders
- getStatus

## Forward vs Redirected

response.sendRedirect

- redirect sets the response status to 302 and the new url in a Location header, and sends the response to the browser
- the browser, according to the http specification, makes another request to the new url
- the url changes and a 300 range code is seen by the client
- slower than a forward bc it has to go all the way back to the server

RequestDispatcher:

- forward
  - request.getRequestDispatcher("Profile.html").forward(request, response);
  - sends the request and response to another resource
  - the browser has no knowledge of this
- include
  - includes any content from the first servlet
  - allows for both sending and receiving servlet to affect

In general, a forward should be used if the operation can be safely repeated upon a browser reload of the resulting web page; otherwise, redirect must be used. Typically, if the operation performs an edit on the datastore, then a redirect, not a forward, is required. This is simply to avoid the possibility of inadvertently duplicating an edit to the database.

- also if you want fresh request/response pairs (login)
