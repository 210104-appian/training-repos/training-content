## Description

The Bank app is a console-based application that simulates banking operations. A customer can apply for an account, view their balance, and make withdrawals and deposits. An employee can approve or deny accounts and view account balances for their customers.

## Purpose

We want to see that you can meet deadlines and that you can code. You are expected to complete the following requirements and give a 5 minute presentation of your project to our QC team.

## Requirements

1. Functionality should reflect the below user stories.
2. Data is stored in a database.
3. Data Access is performed through the use of JDBC in a data layer consisting of Data Access Objects.
4. All input is received using the java.util.Scanner class.
5. Log4j is implemented to log events to a file.
6. JUnit tests are written to test some functionality.

## Required User Stories

- As a user, I can login.
- As a customer, I can apply for a new bank account with a starting balance.
- As a customer, I can view the balance of a specific account.
- As a customer, I can make a withdrawal or deposit to a specific account.
- As the system, I reject invalid transactions.
  - Ex:
    - A withdrawal that would result in a negative balance.
    - A deposit or withdrawal of negative money.
- As an employee, I can approve or reject an account.
- As an employee, I can view a customer's bank accounts.
- A an employee, I can view a log of all transactions.

## Bonus Features

The following is a list of ideas for extending project functionality:

- As a customer, I can transfer money into another account.
- As a customer, I can have a joint account with another customer.
- As a customer, I can take out a loan.
- As a manager, I can make employee accounts.
- As an employee, I can approve a loan.
- As the system, I can calculate the remaining payments required on a loan.
- As a customer, I can make payments on a loan.
- As a manager, I can remove employees from the system.
- As a manager, I can view a log of all actions taken in the app.
- As a user, my password is protected using encryption
- A custom stored procedure is called to perform some portion of the functionality.
