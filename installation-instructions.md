There are several installations you will need to do to work with the technologies we use in training:

- Java 8
- Maven
- Git
- Spring Tool Suite (version 3.x)
- Visual Studio Code (or similar editor of your choosing)

### Java 8

Install the Java 8 Java Development Kit (link found [here](https://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html)), set up appropriate system environment variables, and verify the installation

> the instructions to do so can be found in the tutorials below (detailed in parts I-III):

- [Windows](https://www.oracle.com/webfolder/technetwork/tutorials/OracleCode/Windows-HOL-setup.pdf)
- [Mac](https://www.oracle.com/webfolder/technetwork/tutorials/OracleCode/Mac-HOL-setup.pdf)

If you have other versions of Java other than 8 on your computer, uninstall them (instructions to do so on a windows machine can be found [here](https://www.java.com/en/download/help/uninstall_java.xml))

### Maven

Install Maven set up appropriate system environment variables, and verify the installation (explained in parts VIII-X of the tutorials above)

### Git

Installation and verification are explained in parts VI-VII of the tutorials above.

### Spring Tool Suite

Spring Tool Suite is the IDE that we will be using. It is based off of Eclipse but includes helpful features for when we start working with the Spring Framework.

You can find the most up to date version of STS [here](https://spring.io/tools)

### Visual Studio Code

https://code.visualstudio.com/download

### Additionally you will need access to:

- Revature’s slack workspace
- The batch GitLab group

Slack is the messaging platform used at Revature. It can be used to reach out to me (and is the best way to contact me once training has started), as well as other Revature employees, including your fellow associates. It will be where I send out announcements to everyone, as well as helpful links/code snippets as we go.

We will be using Git for version control along with GitLab. Prior knowledge of Git/GitLab will be very helpful, but is not required, as we will be discussing it during training. For now, though, make sure to create a GitLab account if you do not have one already.

**Please respond to my introductory email with your GitLab username.**

You will be added to the revaturepro slack channel using the email provided, and to a GitLab organization with your username.
